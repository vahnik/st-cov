CREATE SCHEMA IF NOT EXISTS results;
DROP TABLE IF EXISTS results.diff;
CREATE TABLE IF NOT EXISTS results.diff
(
    ticker1 text ,
    ticker2 text,
    total_bars integer,
    correct_diff integer,
    min real,
    max real,
    high_perc real,
    mid_perc real,
    low_perc real,
    PRIMARY KEY (ticker1, ticker2)
)
