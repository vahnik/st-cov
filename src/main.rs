extern crate clap;
extern crate env_logger;
#[macro_use]
extern crate log;

use std::sync::mpsc;
use std::thread;

use clap::App;
use clap::Arg;
use common::combins;
use common::files;
use common::out::Out;
use common::parser;
use time::PreciseTime;
use std::path::Path;
use crate::diff::DiffReport;
use std::collections::HashMap;
use std::collections::BTreeMap;
use std::sync::Arc;
use threadpool::ThreadPool;
use common::domain::period::Period;
use common::domain::bar::Bar;
use std::sync::Mutex;
use itertools::Itertools;
use postgres::Connection;

use r2d2_postgres::PostgresConnectionManager;
use r2d2_postgres::TlsMode;
use postgres::Error;
use std::prelude::v1::Result::Err;
use core::mem;
use std::sync::mpsc::Sender;
use std::sync::mpsc::Receiver;
//use std::error::Error;

mod diff;

fn main() {

    env_logger::init();
    let matches = App::new("stats")
        .version("0.1.0")
        .author("Maksim Vakhnik <m.vakhnik.dev@gmail.com>")
        .usage("stats -f SOURCE_DIR -t TARGET_DIR STATS_TYPE")
        .about(
            "Get stats for bars",
        ).arg(
        Arg::with_name("STATS_TYPE")
            .takes_value(true)
            .required(true)
            .index(1)
            .help(r#"use one of: [DIFF]"#),
    ).arg(
        Arg::with_name("SOURCE_DIR")
            .takes_value(true)
            .long("from")
            .short("f")
            .required(true)
            .help(r#"path to directory with files"#),
    ).arg(
        Arg::with_name("TARGET_DIR")
            .long("to")
            .takes_value(true)
            .required(true)
            .short("t")
            .help("Directory to write results")
    ).arg(
        Arg::with_name("verbose")
            .long("verbose")
            .takes_value(true)
            .short("v")
            .help("Verbosity level: 1 (default), 2")
    ).get_matches();

    let source_dir = matches.value_of("SOURCE_DIR").expect("no source directory");
    debug!("source_dir:{:?}", source_dir);
    let target = matches.value_of("TARGET_DIR").unwrap();
    debug!("target:{:?}", target);
    let opt_verbose = matches.value_of("verbose").unwrap_or("1");
    let _verbose = opt_verbose.parse::<i32>()
        .expect(format!("Incorrect verbose level:{}", opt_verbose).as_str());
    let stats_type = matches.value_of("STATS_TYPE").unwrap();
    let s = PreciseTime::now();
    let paths = combins::paths(source_dir.to_string());

    let all_combs = paths.into_iter().combinations(2);
    let mut total = 0;

    fn parse(t: String) ->  Arc<Mutex<BTreeMap<Period, Bar>>> {
        Arc::new((Mutex::new(parser::parse_from_file(&t))))
    }
    let cache = &mut HashMap::new();




    let (tx_counter, rx_counter) = mpsc::channel();

    let (tx_result, rx_result): (Sender<DiffReport>, Receiver<DiffReport>) = mpsc::channel();
    let target_dir = target.to_owned();

    fn to_psql(dr: DiffReport, conn: Connection) -> Result<u64, Box<Error>>{
        let res = (conn.execute("INSERT INTO results.diff (ticker1, ticker2,
            total_bars, correct_diff, min, max, high_perc,
            mid_perc, low_perc) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)",
     &[&dr.t1, &dr.t2, &dr.total_bars, &dr.correct_diff, &dr.min, &dr.max, &dr.high_perc(),
            &dr.mid_perc(), &dr.low_perc()]))?;
        Ok(res)
    }

    let manager = PostgresConnectionManager::new("postgres://postgres@localhost:5432/analyze",
                                                 TlsMode::None).unwrap();
    let builder = r2d2_postgres::r2d2::Builder::new();
    let db_pool = builder.max_size(20).build(manager).unwrap();
//    let db_pool = r2d2_postgres::r2d2::Pool::new(manager).unwrap();
    let st =db_pool.state();
    let _writer = thread::spawn(move||{

        let pool = ThreadPool::new(12);
        for dr in rx_result {
            let db_pool = db_pool.clone();
            let tx_counter = tx_counter.clone();
            pool.execute(move||{
                let conn = db_pool.get().unwrap();
                let res = conn.execute("INSERT INTO results.diff (ticker1, ticker2,
                total_bars, correct_diff, min, max, high_perc,
                mid_perc, low_perc) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)",
                                            &[&dr.t1, &dr.t2, &dr.total_bars, &dr.correct_diff, &dr.min, &dr.max, &dr.high_perc(),
                                                &dr.mid_perc(), &dr.low_perc()]);
                    tx_counter.send(1).expect("cannot send counter value from writer");
            });


        }
    });

    if stats_type == "DIFF" {

        let s = PreciseTime::now();
        let pool = ThreadPool::new(12);

        let mut counter = 0;

        for c in all_combs {
            counter = counter + 1;
            if counter % 5000 == 0 {
                println!("current:{}", counter);
            }
            let file0: String = c.get(0).expect("Cannot get ticker 0").clone();
            let file1: String = c.get(1).expect("Cannot get ticker 1").clone();
            let t0 = file0.clone();
            let t1 = file1.clone();
            let e0 = cache.entry(t0.clone()).or_insert_with(||parse(t0)).clone();
            let arc0 = Arc::clone(&e0);
            let e1 = cache.entry(t1.clone()).or_insert_with(||parse(t1)).clone();
            let arc1 = Arc::clone(&e1);
            let txc = tx_result.clone();
            let ticker0 = files::fullpath_to_filename_without_ext(file0);
            let ticker1 = files::fullpath_to_filename_without_ext(file1);
            pool.execute(move || {

                let m0 = arc0.lock().unwrap();
                let m1 = arc1.lock().unwrap();
                let bars = vec![m0, m1];
                let result = diff::calc(ticker0, ticker1, bars);
                txc.send(result).expect("cannot send result to writer");
            });
        }

        total = counter;
        println!("Start waiting for results:{}", total);
        for received in rx_counter {
            debug!("received:{} {}", counter, received);
            if counter % 5000 == 0 {
                println!("remained:{}", counter);
            }
            counter = counter - 1;
            if counter == 0 {
                println!("END");
                break;
            }
        }
        println!(
            "total: {}, full calc time:{}, ms",
            total,
            s.to(PreciseTime::now()).num_milliseconds()
        );
    }


}

