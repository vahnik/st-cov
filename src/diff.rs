use std::collections::BTreeMap;

use common::combins;
use common::domain::bar::Bar;
use common::domain::period::Period;
use common::parser;
use common::normalizer;
use common::out::Out;
use common::files;
use std::sync::Mutex;
use std::sync::MutexGuard;

//pub struct Diff {
//    pub source_dir: String,
//    pub target_dir: String
//
//}
#[derive(Debug, Clone)]
pub struct DiffResult {
    pub period: Period,
    pub p1: f32,
    pub p2: f32,
    pub diff: f32,
    pub diff_avg: f32
}

impl DiffResult {
    pub fn to_string(&self) -> String {
        format!("{}-{}-{},{},{},{},{}",
            self.period.year,
                self.period.month,
                self.period.day,
                self.p1, self.p2, self.diff, self.diff_avg)
    }
}
#[derive(Debug, Clone)]
pub struct DiffReport {
    pub t1: String,
    pub t2: String,
    pub diff_results: Vec<DiffResult>,
    pub total_bars: i32,
    pub correct_diff: i32,
//    pub intersected: i32,
//    pub intersec_freq: f32,
    pub highest: Vec<f32>,
    pub middle: Vec<f32>,
    pub lowest: Vec<f32>,
    pub min: f32,
    pub max: f32
}

impl DiffReport {
    pub fn headers() -> String {
        "ticker1,ticker2,total_bars,correct_diff,max,min,high_perc,mid_perc,lowe_perc".to_string()
    }
    pub fn to_csv(&self) -> String {

        format!("{},{},{},{},{},{},{},{},{}",
                self.t1, self.t2,
                self.total_bars, self.correct_diff, self.max, self.min,
                self.high_perc(),
                self.mid_perc(),
                self.low_perc()
        )
    }
    pub fn high_perc(&self) -> f32 {
        self.highest.len() as f32/ (self.correct_diff as f32 / 100.0)
    }
    pub fn mid_perc(&self) -> f32{
        self.middle.len() as f32/ (self.correct_diff as f32 / 100.0)
    }
    pub fn low_perc(&self) -> f32{
        self.lowest.len() as f32/ (self.correct_diff as f32 / 100.0)
    }
}
fn to_diff_report(t1: String, t2: String, diff_results: Vec<DiffResult>, total_bars: i32) -> DiffReport{
    let correct_diff = diff_results.len() as i32;
    let mut min= 0.0;
    let mut max= 0.0;
    let mut all = Vec::new();
    let mut highest = Vec::new();
    let mut middle = Vec::new();
    let mut lowest = Vec::new();
    for dr in diff_results.to_vec() {
        let c_diff = dr.diff;
        if c_diff < min {min = c_diff};
        if c_diff > max {max = c_diff};
        all.push(c_diff);
    }
    let range = max - min;
    let area = range / 4.0;
    let high_bottom_edge = max - area;
    let low_top_edge = min + area;
    for c in all {

        if c < low_top_edge {
            lowest.push(c);
        } else if c < high_bottom_edge {
             middle.push(c);
        } else {
            highest.push(c);
        }
    }
    DiffReport{t1, t2, diff_results, total_bars, correct_diff, highest, middle, lowest, min, max}
}
pub fn calc(ticker1: String , ticker2: String, bars: Vec<MutexGuard<BTreeMap<Period, Bar>>>) -> DiffReport {

    let period = 7;
    let mut diff_results = Vec::new();
//            let printer = &mut Out::new(Some(fullname.as_str()));


    let bars_vec1: &BTreeMap<Period, Bar> = bars.get(0)
        .expect(format!("cannot find 0 bars series in :{:?}", ticker1).as_str());
    let bars_vec2: &BTreeMap<Period, Bar> = bars.get(1)
        .expect(format!("cannot find 1 bars series in:{:?}", ticker2).as_str());//
    let mut stack = Vec::with_capacity(period);
    for bars1 in bars_vec1 {
        let b2 = bars_vec2.get(bars1.0);

        let diff_result = calc_diff(bars1.0.clone(), bars1.1, b2, &stack);
        if stack.len() == period {
            stack.remove(0);
        }
        stack.push(diff_result.clone());
        //     printer.print(as_string(&diffResult));
        diff_results.push(diff_result);
    }
//        }
    let len0 = bars.get(0).expect("no bars 0").len();
    let len1 = bars.get(1).expect("no bars 1").len();
    let total_bars = if len0 >  len1 {
        len0
    } else {
        len1
    };
    to_diff_report(ticker1, ticker2, diff_results, total_bars as i32)


}

fn calc_diff(period: Period, b1: &Bar, b2: Option<&Bar>, prev_for_avg: &Vec<DiffResult>) -> DiffResult {
    let p1 = b1.price();
    let p2 = b2.map_or(0_f32, |b| b.price());
    let diff = p1 - p2;
    let mut diff_avg = 0_f32;
    for prev in prev_for_avg {
        diff_avg = diff_avg + prev.diff;
    }
    if prev_for_avg.len() > 0 {
        diff_avg = diff_avg / prev_for_avg.len() as f32;
    }

    DiffResult{period, p1, p2, diff, diff_avg}


}



//}

